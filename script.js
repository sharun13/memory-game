const gameContainer = document.getElementById("game");
const results = document.getElementById('final-result');
const replay = document.getElementById('replay');
const winMsg = document.getElementById('win-msg');
const moves = document.getElementById('moves');

const COLORS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "red",
  "blue",
  "green",
  "orange",
  "purple",
];

function shuffleItems(array) {
  let counter = array.length;
  while (counter > 0) {
    let index = Math.floor(Math.random() * counter);
    counter--;
    let tempVariable = array[counter];
    array[counter] = array[index];
    array[index] = tempVariable;
  }
  return array;
}

let colorsArray = [...COLORS, ...COLORS];
let shuffledColors = shuffleItems(colorsArray);
let counter = 0;
let primary = "", secondary = "";

function cardGenerator(colorsArray) {
  for (let color of colorsArray) {
    const newDiv = document.createElement("div");
    newDiv.classList.add(color);
    newDiv.style.backgroundColor = "grey";
    newDiv.style.cursor = 'pointer';
    newDiv.addEventListener("click", onCardClick);
    gameContainer.append(newDiv);
  }
}

let totalCount = 0;
let movesCount = 0;
function onCardClick(event) {

  movesCount++;
  moves.innerHTML = `Moves : ${movesCount}`;
  let color = event.target.className;
  if (event.target.style.backgroundColor === "grey") {
    event.target.style.backgroundColor = color;
  }
  if (counter === 0) {
    primary = event.target;
    counter++;
    event.target.removeEventListener('click', onCardClick);
  }
  else {
    secondary = event.target;
    counter = 0;

    if (primary.style.backgroundColor !== secondary.style.backgroundColor) {
      document.querySelector('#game').style.pointerEvents = 'none';
      setTimeout(() => {
        primary.style.backgroundColor = "grey";
        primary.addEventListener('click', onCardClick);
        secondary.style.backgroundColor = "grey";
        event.preventDefault();
        document.querySelector('#game').style.pointerEvents = 'auto';
      }, 1000);
    }
    if (primary.style.backgroundColor == secondary.style.backgroundColor) {
      totalCount += 1;
      primary.style.pointerEvents = 'none';
      secondary.style.pointerEvents = 'none';
    }
  }

  if (totalCount == 10) {
    results.innerHTML = "Congratulations! You Won!"
    replay.style.display = 'block';
    winMsg.innerHTML = `Your Score : ${movesCount}`;
    moves.innerHTML = `Moves : ${0}`;
    moves.style.display = 'none';
  }
}

cardGenerator(colorsArray);

